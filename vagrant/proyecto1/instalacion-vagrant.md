	
Install Vagrant that is wrapper tool for Virtualization software like Libvirt or VirtualBox and others.
Vagrant supports many Virtualization software like VirtualBox or Libvirt and others,
this example is based on the Libvirt installed environment.
[1]	Install Vagrant.
root@dlp:~# apt -y install vagrant-libvirt
# if use by common users, add them to [libvirt] group
root@dlp:~# usermod -aG libvirt debian
[2]	Basic usage of Vagrant. It's possible to use it by any common users who are in [libvirt] group.
# download and add virtual machine images
# for downloadable image, refer to the official site below
# ⇒ https://app.vagrantup.com/boxes/search
debian@dlp:~$ vagrant box add generic/debian10 --provider libvirt
==> box: Loading metadata for box 'generic/debian10'
    box: URL: https://vagrantcloud.com/generic/debian10
==> box: Adding box 'generic/debian10' (v3.4.0) for provider: libvirt
    box: Downloading: https://vagrantcloud.com/generic/boxes/debian10/versions/3.4.0/providers/libvirt.box
Download redirected to host: vagrantcloud-files-production.s3-accelerate.amazonaws.com
    box: Calculating and comparing box checksum...
==> box: Successfully added box 'generic/debian10' (v3.4.0) for 'libvirt'!

# initialize ([Vagrantfile] is created on the current path)
debian@dlp:~$ vagrant init generic/debian10
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.

# start virtual machine
debian@dlp:~$ vagrant up
Bringing machine 'default' up with 'libvirt' provider...
==> default: Checking if box 'generic/debian10' version '3.4.0' is up to date...
==> default: Uploading base box image as volume into Libvirt storage...
==> default: Creating image (snapshot of base box volume).
==> default: Creating domain with the following settings...
==> default:  -- Name:              debian_default
==> default:  -- Domain type:       kvm
==> default:  -- Cpus:              2
==> default:  -- Feature:           acpi
==> default:  -- Feature:           apic
==> default:  -- Feature:           pae
==> default:  -- Memory:            2048M
==> default:  -- Management MAC:
==> default:  -- Loader:
==> default:  -- Nvram:
==> default:  -- Base box:          generic/debian10
==> default:  -- Storage pool:      default
==> default:  -- Image:             /var/lib/libvirt/images/debian_default.img (128G)
==> default:  -- Volume Cache:      default
==> default:  -- Kernel:
==> default:  -- Initrd:
==> default:  -- Graphics Type:     vnc
==> default:  -- Graphics Port:     -1
==> default:  -- Graphics IP:       127.0.0.1
==> default:  -- Graphics Password: Not defined
==> default:  -- Video Type:        cirrus
==> default:  -- Video VRAM:        256
==> default:  -- Sound Type:
==> default:  -- Keymap:            en-us
==> default:  -- TPM Path:
==> default:  -- INPUT:             type=mouse, bus=ps2
==> default: Creating shared folders metadata...
==> default: Starting domain.
==> default: Waiting for domain to get an IP address...
==> default: Waiting for SSH to become available...
    default:
    default: Vagrant insecure key detected. Vagrant will automatically replace
    default: this with a newly generated keypair for better security.
    default:
    default: Inserting generated public key within guest...
    default: Removing insecure key from the guest if it's present...
    default: Key inserted! Disconnecting and reconnecting using new SSH key...

# show state of virtual machine
debian@dlp:~$ vagrant status
Current machine states:

default                   running (libvirt)

The Libvirt domain is running. To stop this machine, you can run
`vagrant halt`. To destroy the machine, you can run `vagrant destroy`.

# connect to virtual machine with SSH
debian@dlp:~$ vagrant ssh

vagrant@debian10:~$ uname -a 
Linux debian10.localdomain 4.19.0-17-amd64 #1 SMP Debian 4.19.194-3 (2021-07-18) x86_64 GNU/Linux

vagrant@debian10:~$ exit 

# stop virtual machine
debian@dlp:~$ vagrant halt
==> default: Halting domain...
# if you'd like to change settings of virtual machine, edit Vagrantfile
debian@dlp:~$ vi Vagrantfile
# for example to change CPU and Memory settings
# uncomment line 52 like follows and add or change values
  config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
     vb.memory = "4096"
     vb.cpus = 2
  end