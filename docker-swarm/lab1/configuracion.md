Docker : Swarm Cluster2021/08/30
 	
Configure Docker Swarm to create Docker Cluster with multiple Docker nodes.
On this example, Configure Swarm Cluster with 3 Docker nodes like follows.
There are 2 roles on Swarm Cluster, those are [Manager nodes] and [Worker nodes].
This example shows to set those roles like follows.
 -----------+---------------------------+--------------------------+------------
            |                           |                          |
        eth0|10.0.0.51              eth0|10.0.0.52             eth0|10.0.0.53
 +----------+-----------+   +-----------+----------+   +-----------+----------+
 | [ node01.srv.world ] |   | [ node02.srv.world ] |   | [ node03.srv.world ] |
 |       Manager        |   |        Worker        |   |        Worker        |
 +----------------------+   +----------------------+   +----------------------+

[1]	
Install and run Docker service on all nodes, refer to here.
[2]	Disable live-restore feature on all Nodes.
(because it can not use live-restore feature on Swarm mode)
root@node01:~# vi /etc/docker/daemon.json
# create new
{
    "live-restore": false
}

[root@node01 ~]# systemctl restart docker
[3]	Configure Swarm Cluster on Manager Node.
root@node01:~# docker swarm init
Swarm initialized: current node (zz1godpx4shbsotcs0ppvkfco) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-21cjy7xak542vkxkx55nhw2ld65xpp349hsx990rp1n2aocmn2-1rtmzdt9b0tgxe1hiuj5rh555 10.0.0.51:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
[4]	Join in Swarm Cluster on all Worker Nodes.
It's OK to run the command which was shown when running swarm init on Manager Node.
root@node02:~# docker swarm join \
--token SWMTKN-1-21cjy7xak542vkxkx55nhw2ld65xpp349hsx990rp1n2aocmn2-1rtmzdt9b0tgxe1hiuj5rh555 10.0.0.51:2377
This node joined a swarm as a worker.
[5]	Verify with a command [node ls] that worker nodes could join in Cluster normally.
root@node01:~# docker node ls
ID                            HOSTNAME           STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
zz1godpx4shbsotcs0ppvkfco *   node01.srv.world   Ready     Active         Leader           20.10.5+dfsg1
76diovgr36pcudov4gf3apkgi     node02.srv.world   Ready     Active                          20.10.5+dfsg1
7kyu2gqkuuf32esv1swi4py3a     node03.srv.world   Ready     Active                          20.10.5+dfsg1
[6]	After creating Swarm Cluster, configure services the Swarm Cluster provides.
For example, create a Nginx container to configure Swarm service.
Generally, it is used a container image on a rgistry on all Nodes, however on this example, create container images on each Node to verify settings and accesses for Swarm Cluster.
root@node01:~# vi Dockerfile
FROM debian
MAINTAINER ServerWorld <admin@srv.world>

RUN apt-get update
RUN apt-get -y install nginx
RUN echo "Nginx on node01" > /var/www/html/index.html

EXPOSE 80
CMD ["/usr/sbin/nginx", "-g", "daemon off;"]

root@node01:~# docker build -t nginx-server:latest .
[7]	Configure service on Manager Node.
After successing to configure service, access to the Manager node's Hostname or IP address to verify it works normally. Access requests to worker nodes are load-balanced with round-robin like follows.
root@node01:~# docker images
REPOSITORY     TAG       IMAGE ID       CREATED              SIZE
nginx-server   latest    277e68de6963   About a minute ago   210MB
debian         latest    fe3c5de03486   13 days ago          124MB

# create a service with 2 repricas
root@node01:~# docker service create --name swarm_cluster --replicas=2 -p 80:80 nginx-server:latest
xnfelfmy558z22dfy5hx1q1sw
overall progress: 2 out of 2 tasks
1/2: running
2/2: running
verify: Service converged
# show service list
root@node01:~# docker service ls
ID             NAME            MODE         REPLICAS   IMAGE                 PORTS
xnfelfmy558z   swarm_cluster   replicated   2/2        nginx-server:latest   *:80->80/tcp

# inspect the service
root@node01:~# docker service inspect swarm_cluster --pretty

ID:             xnfelfmy558z22dfy5hx1q1sw
Name:           swarm_cluster
Service Mode:   Replicated
 Replicas:      2
Placement:
UpdateConfig:
 Parallelism:   1
 On failure:    pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Update order:      stop-first
RollbackConfig:
 Parallelism:   1
 On failure:    pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Rollback order:    stop-first
ContainerSpec:
 Image:         nginx-server:latest
 Init:          false
Resources:
Endpoint Mode:  vip
Ports:
 PublishedPort = 80
  Protocol = tcp
  TargetPort = 80
  PublishMode = ingress

# show service state
root@node01:~# docker service ps swarm_cluster
ID             NAME              IMAGE                 NODE               DESIRED STATE   CURRENT STATE                ERROR     PORTS
r4fqd0wkgmzb   swarm_cluster.1   nginx-server:latest   node02.srv.world   Running         Running about a minute ago            
qpj91te544c3   swarm_cluster.2   nginx-server:latest   node01.srv.world   Running         Running about a minute ago    

# verify it works normally
root@node01:~# curl node01.srv.world
Nginx on node02
root@node01:~# curl node01.srv.world
Nginx on node01
root@node01:~# curl node01.srv.world
Nginx on node02
root@node01:~# curl node01.srv.world
Nginx on node01
[8]	If you'd like to change the number of repricas, configure like follows.
# change repricas to 3
root@node01:~# docker service scale swarm_cluster=3
swarm_cluster scaled to 3
overall progress: 3 out of 3 tasks
1/3: running
2/3: running
3/3: running
verify: Service converged
root@node01:~# docker service ps swarm_cluster
ID             NAME              IMAGE                 NODE               DESIRED STATE   CURRENT STATE            ERROR     PORTS
r4fqd0wkgmzb   swarm_cluster.1   nginx-server:latest   node02.srv.world   Running         Running 2 minutes ago
qpj91te544c3   swarm_cluster.2   nginx-server:latest   node01.srv.world   Running         Running 2 minutes ago
pohrmr7dn2en   swarm_cluster.3   nginx-server:latest   node03.srv.world   Running         Running 23 seconds ago

# verify accesses
root@node01:~# curl node01.srv.world
Nginx on node03
root@node01:~# curl node01.srv.world
Nginx on node02
root@node01:~# curl node01.srv.world
Nginx on node01