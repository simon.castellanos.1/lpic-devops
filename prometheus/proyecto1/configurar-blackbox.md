 	
To configure Blackbox exporter, it's possible to probe endpoints over HTTP, HTTPS, DNS, TCP and ICMP.
[1]	On a Node you'd like to monitor with Blackbox exporter, Install it.
root@node01:~# apt -y install prometheus-blackbox-exporter
[2]	This is the setting file of Blackbox exporter. (Keep default on this example)
root@node01:~# vi /etc/prometheus/blackbox.yml
modules:
  http_2xx:
    prober: http
  http_post_2xx:
    prober: http
    http:
      method: POST
  tcp_connect:
    prober: tcp
  pop3s_banner:
    prober: tcp
    tcp:
      query_response:
      - expect: "^+OK"
      tls: true
      tls_config:
        insecure_skip_verify: false
  ssh_banner:
    prober: tcp
    tcp:
      query_response:
      - expect: "^SSH-2.0-"
  irc_banner:
    prober: tcp
    tcp:
      query_response:
      - send: "NICK prober"
      - send: "USER prober prober prober :prober"
      - expect: "PING :([^ ]+)"
        send: "PONG ${1}"
      - expect: "^:[^ ]+ 001"
  icmp:
    prober: icmp

root@node01:~# systemctl enable prometheus-blackbox-exporter
[3]	Add settings on Prometheus Server Node.
root@dlp:~# vi /etc/prometheus/prometheus.yml
.....
.....
scrape_configs:
  # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
  - job_name: 'prometheus'

.....
.....

  # the case to use [icmp] module
  # any [job_name]
  - job_name: 'Blackbox_icmp'
    metrics_path: /probe
    params:
      module: [icmp]
    static_configs:
      - targets:
        # hostname or IP address of target Host
        - node01.srv.world
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        # Blackbox exporter Host:Port
        replacement: node01.srv.world:9115

  # the case to use [ssh_banner] module
  - job_name: 'Blackbox_ssh'
    metrics_path: /probe
    params:
      module: [ssh_banner]
    static_configs:
      - targets:
        # target Host:Port
        - node01.srv.world:22
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: node01.srv.world:9115

  # the case to use [tcp_connect] module
  - job_name: 'Blackbox_tcp'
    metrics_path: /probe
    params:
      module: [tcp_connect]
    static_configs:
      - targets:
        # target Host:Port (example below is Apache2)
        - node01.srv.world:80
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: node01.srv.world:9115 

root@dlp:~# systemctl restart prometheus
[4]	Access to the Prometheus Web UI and move to [Status] - [Targets], then new configured targets are shown.
It's possible to see data on [probe_success] metric. [1] means success, [0] means fail.


