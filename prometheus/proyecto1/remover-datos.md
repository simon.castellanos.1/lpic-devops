Prometheus : Remove Data2021/09/17
 	
If you'd like to remove time series data on Prometheus, set like follows. (Prometheus 2.1 or later)
This is the deletion API.
⇒ http://prometheus/api/v2/admin/tsdb/delete_series
[1]	Enable Admin API first to use deletion API.
root@dlp:~# vi /etc/default/prometheus
# add
ARGS="--web.enable-admin-api"
root@dlp:~# systemctl restart prometheus
[2]	This is some examples of removing Data.
# remove data that [job] name matches [Blackbox_tcp]
root@dlp:~# curl -X POST -g 'http://dlp.srv.world:9090/api/v1/admin/tsdb/delete_series?match[]={job="Blackbox_tcp"}'
# remove data that [instance] name matches [node02.srv.world]
root@dlp:~# curl -X POST -g 'http://dlp.srv.world:9090/api/v1/admin/tsdb/delete_series?match[]={instance="node02.srv.world"}'
# remove all data
root@dlp:~# curl -X POST -g 'http://dlp.srv.world:9090/api/v1/admin/tsdb/delete_series?match[]={__name__=~".+"}'
# removed data are actually still on disk but if you'd like to clean up them manually, run like follows
root@dlp:~# curl -X POST -g 'http://dlp.srv.world:9090/api/v1/admin/tsdb/clean_tombstones'