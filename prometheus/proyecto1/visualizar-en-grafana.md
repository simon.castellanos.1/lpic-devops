Web UI is included in Prometheus but it's also possible to visualize time series data on Grafana.
[1]	
Install Grafana, refer to here.
It's OK to install it on any Node. (install it on Prometheus server Node on this example)
[2]	Access to Grafana Dashboard and Open [Configuration] - [Data Sources] on the left menu.

[3]	Click [Add data source].

[4]	Click [Prometheus].

[5]	Input Prometheus Server endpoint URL on [HTTP] field and Click [Save & Test] button.
Then, the message [Data source is working] is shown if that's OK.

[6]	Next, Open [Create] - [Dashboard] on the left menu.

[7]	Click [Add Query].

[8]	Select a query you'd like to visualize data on [Metrics] field.

[9]	After selecting a query, visualized graph is shown. To save [Dashboard], Click [Save dashboard] icon.

[10]	Set any Dashboard name and Click [Save] button.

[11]	To add more queries, it's possible to put more Graphs on a Dashboard.