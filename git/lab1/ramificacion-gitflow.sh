
SCRIPT PARA PROBAR GITFLOW
----------------------------

PLANILLA EXCEL
----------------
NOMBRE REPO, URL, Tipo Commit, Usuario,  master, develop, feature-J000-001, feature-J000-002, release, RESULTADO.
------------------------------------------------------------------------------------------------------------------


Paso 1)
git clone http://192.168.50.20:8081/programador1/semanticdemo_2.git
o 
git pull



Paso 2)
git branch -a
* develop
  master
  remotes/origin/HEAD -> origin/master
  remotes/origin/develop
  remotes/origin/feature-J0078-1730
  remotes/origin/feature-J0078-1827
  remotes/origin/master
  remotes/origin/release


Paso 3)
git checkout develop
git pull origin develop



Paso 4) 
git checkout -b feature-J002-002 develop
Switched to a new branch 'feature-J001-001'



Paso 5)  Generar etiqueta de feature. 
touch README-feature-J002-002.md
git add .
git commit -m "fix: Ajustes de variables"
//git push 
git push --set-upstream origin feature-J001-001
$ git push --set-upstream origin feature-J001-001
Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
Delta compression using up to 8 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (2/2), 294 bytes | 294.00 KiB/s, done.
Total 2 (delta 1), reused 0 (delta 0), pack-reused 0
remote:
remote: To create a merge request for feature-J001-001, visit:
remote:   http://localhost/programador1/semanticdemo_2/-/merge_requests/new?merge_request%5Bsource_branch%5D=feature-J001-001
remote:
To http://192.168.50.20:8081/programador1/semanticdemo_2.git
 * [new branch]      feature-J001-001 -> feature-J001-001
branch 'feature-J001-001' set up to track 'origin/feature-J001-001'.



Paso 6) Fusionar con develop para versiones Alpha
#opcion 1 - Asegura pull request
#git merge --no-ff feature-J001-001
git checkout develop
git pull
git merge feature-J002-002
------------------------------------------------------------------
scastech@SAN-D5VGJQ3 MINGW64 ~/semanticdemo_2 (develop)
$ git merge feature-J001-001
Auto-merging CHANGELOG.md
CONFLICT (content): Merge conflict in CHANGELOG.md
Auto-merging VERSION.txt
CONFLICT (content): Merge conflict in VERSION.txt
Automatic merge failed; fix conflicts and then commit the result.
-------------------------------------------------------------------------
SOLUCION

<<<<<<< HEAD
Contenido en la rama actual
=======
Contenido en la rama que estás fusionando
>>>>>>> feature-J001-001


git add CHANGELOG.md VERSION.txt  # Agrega los archivos con conflictos después de resolverlos
git commit -m "Resuelve conflictos de fusión en CHANGELOG.md y VERSION.txt"


git merge --continue
git push origin develop  # O la rama en la que estés trabajando
---------------------------------------

El mensaje de error indica que hay conflictos de fusión que necesitan ser resueltos antes de poder cambiar a la rama master. 
Aquí hay una serie de pasos que puedes seguir para resolver estos conflicto
---------------------
git mergetool
git add .releaserc.yml CHANGELOG.md VERSION.txt contenido.txt
git merge --continue
git checkout master
git commit -m "Finalizar fusión en master"
git push origin master
------------------------

git mergetool
:wq!
git add .
scastech@SAN-D5VGJQ3 MINGW64 ~/semanticdemo_2 (develop|MERGING)
$ git status
On branch develop
Your branch is up to date with 'origin/develop'.

All conflicts fixed but you are still merging.
  (use "git commit" to conclude merge)

Changes to be committed:
        modified:   CHANGELOG.md
        new file:   CHANGELOG.md.orig
        modified:   Jenkinsfile
        new file:   README-feature-J001-001.md
        modified:   VERSION.txt
        new file:   VERSION.txt.orig

scastech@SAN-D5VGJQ3 MINGW64 ~/semanticdemo_2 (develop|MERGING)
git merge --continue

scastech@SAN-D5VGJQ3 MINGW64 ~/semanticdemo_2 (develop)
$ git checkout feature-J001-001
Switched to branch 'feature-J001-001'
Your branch is up to date with 'origin/feature-J001-001'.
--
scastech@SAN-D5VGJQ3 MINGW64 ~/semanticdemo_2 (develop|MERGING)
$ git merge --continue
[develop fdea6a8] Merge branch 'feature-J001-001' into develop
 Committer: Simon Castellanos Chauran <scastech@emeal.nttdata.com>
Your name and email address were configured automatically based
on your username and hostname. Please check that they are accurate.
You can suppress this message by setting them explicitly:

    git config --global user.name "Your Name"
    git config --global user.email you@example.com

After doing this, you may fix the identity used for this commit with:

    git commit --amend --reset-author


scastech@SAN-D5VGJQ3 MINGW64 ~/semanticdemo_2 (develop)
$ git checkout feature-J001-001
Switched to branch 'feature-J001-001'
Your branch is up to date with 'origin/feature-J001-001'.

scastech@SAN-D5VGJQ3 MINGW64 ~/semanticdemo_2 (feature-J001-001)
$ git commit -m "Finalizar fusión en feature->develop"
On branch feature-J001-001
Your branch is up to date with 'origin/feature-J001-001'.

nothing to commit, working tree clean

scastech@SAN-D5VGJQ3 MINGW64 ~/semanticdemo_2 (feature-J001-001)
$ git push origin master















#git commit -m "fix: Fusion de las ramas" (si es necesario)

git pull
git tag 
git push origin develop
git push origin --tags
[2:45:17 AM] [semantic-release] › ✔  Published release 1.2.0-feature-j002-002.1 on feature-J002-002 channel
























=======================================================

# Comandos git


v1.2.0-feature-j0078-1730.1-->v1.2.0-alpha.1-->



Feature?
Develop?
Release?
Master?

v2.0.0-alpha.1-->v2.0.0-beta.1-->v2.0.0






Por supuesto, aquí están los 8 pasos anteriores con la modificación de usar una etiqueta en lugar de una rama de lanzamiento:

#1. **Crear rama principal (master):**
   ```bash
   git branch master
   git checkout master
   ```

#2. **Crear rama de desarrollo (develop) desde la rama principal:**
   ```bash
   git checkout -b develop
   ```

#3. **Crear rama de característica (feature/<nombre>) desde la rama de desarrollo:**
   ```bash
   git checkout -b feature/nueva-caracteristica develop
   ```
   Realiza cambios y commits en esta rama.
   

   
   
   
   

#4. **Fusionar rama de característica en la rama de desarrollo:**
   ```bash
   git checkout develop
   git merge --no-ff feature/nueva-caracteristica
   ```

#5. **Crear etiqueta para la versión (en lugar de rama de lanzamiento):**
   ```bash
   git tag -a v1.0.0 -m "Versión 1.0.0"
   ```

#6. **Implementar etiqueta en todos los entornos:**
   Despliega la versión marcada por la etiqueta en los entornos correspondientes.

#7. **Fusionar etiqueta en la rama principal y la rama de desarrollo:**
   ```bash
   git checkout master
   git merge --no-ff v1.0.0
   git checkout develop
   git merge --no-ff v1.0.0
   ```

#8. **Eliminar ramas de características después de la fusión:**
   ```bash
   git branch -d feature/nueva-caracteristica
   ```

Estos son los pasos modificados para reflejar el uso de una etiqueta en lugar de una rama de lanzamiento. Recuerda ajustar las versiones y nombres de las ramas según las necesidades específicas de tu proyecto.
