#!/bin/bash
#-------------------------------------------------------------------
# Programa: manejar-gitflow.sh
# Autor:   Simon Castellanos
# Fecha:   28/12/2023
# Entorno:  Windows 10/11
# Descripcion: 
#   Permite trabajar ramas feature-** en  repositorio local git y
#   sincronizarlo en el repo git remoto
#-------------------------------------------------------------------
 
# --- Verificar la existencia de rama feature-** #
verificar_rama_feature() {
  git pull origin develop
  # Parámetros de entrada
  local version=$1
  local feature_name=$2
  # Verificar si existe la etiqueta
  if git rev-parse -q --verify "v$version-feature-$feature_name" >/dev/null; then
    return 0  # Retorna true (0) si la etiqueta existe
  else
    return 1  # Retorna false (1) si la etiqueta no existe
  fi
}
#----------------------------------------------#
# Trabajar en una característica (feature-**)
#----------------------------------------------#
function version_feature() {
    ultima_version_rama
    read -p "Ingrese la versión (ej. 1.0.0): " version
    read -p "Ingrese el nombre de la característica (ej. J001): " feature_name
    verificar_rama_feature "$version" "$feature_name"
    if [ $? -eq 0 ]; then
      echo "La etiqueta o rama ya existe."
    else
      echo "La etiqueta no existe. Creando rama feature-**"
      git checkout develop
      git checkout -b "feature-$feature_name"
      touch README-feature-$feature_name.txt
      git add .
      git commit -m "Checkout para implementar la funcionalidad  feature-$feature_name"
      #git tag -a v$version-feature-$feature_name -m "Versión $version-$feature_name"
      git log -1 --oneline
      ## git branch --set-upstream-to=origin/<branch> feature-J004
      git push --set-upstream origin feature-$feature_name
	  $rama="feature-$feature_name"
	  echo "--------------------------------------------------"
	  echo " Nueva etiqueta en rama: $rama"
	  echo "--------------------------------------------------"
     fi
}
# --- Verificar prerelease alpha o beta --#
verificar_prerelease() {
  # Parámetros de entrada
  local version=$1
  local prerelease=${2:-"-alpha"}  # Valor predeterminado es "-alpha" si no se proporciona
  # Verificar si existe la etiqueta
  if git rev-parse -q --verify "v$version$prerelease" >/dev/null; then
    echo "La etiqueta v$version$prerelease ya existe."
    return 0
  else
    echo "La etiqueta v$version$prerelease no existe."
    return 1
  fi
}
#------------------------------------#
#  Generar versión Develop (*-alpha)
#------------------------------------#
function version_develop() {
    ultima_version_rama
    read -p "Ingrese la versión (ej. 1.0.0): " version
    read -p "Ingrese el nombre de la característica (ej. J001): " feature_name
    prerelease="-alpha"
    verificar_prerelease "$version" "$prerelease"
    if [ $? -eq 0 ]; then
      echo "La etiqueta alpha o fusion ya existe."
    else
      echo "La etiqueta Alpha no existe. Fusionando feature-** en develop"
      git checkout develop
      git merge --no-ff feature-$feature_name -m "Merge de rama feature-** en develop - version Alpha "
      #git tag -a v$version$prerelease -m "Versión $version$prerelease"
      git log -1 --oneline
      #git pull --all
      #git push --all
      rama=$(git branch --show-current)
      git pull origin $rama
      git push --set-upstream origin $rama
      source_branch=feature-$feature_name
	  target_branch="develop"
	  url=$(generar_merge_request_url "$source_branch" "$target_branch")
	  echo "----------------------------------------------------------"
	  echo " Pull Request para version pre-release ALPHA  "
	  echo " URL Solicitud de Fusion: $url"
	  echo "----------------------------------------------------------"
	  # Luego de aplicar el Request Merge se etiqueta v.1.2.3-beta.1
    fi
}
# --- Verificar la existencia de rama release --#
function verificar-ramarelease() {
    branch="release"
    # Verificar si la rama 'release' ya existe
    if git show-ref --quiet refs/heads/$branch; then
        echo "La rama '$branch' ya existe."
        git checkout $branch
    else
        echo "La rama '$branch' no existe. Creando..."
        git checkout -b $branch
    fi
}
#------------------------------------#
#  Generar versión Release (*-beta)
#------------------------------------#
function version_release() {
    ultima_version_rama
    read -p "Ingrese la versión (ej. 1.0.0): " version
    prerelease="-beta"
    verificar_prerelease "$version" "$prerelease"
    if [ $? -eq 0 ]; then
      echo "La etiqueta beta o fusion ya existe."
    else
      echo "La etiqueta Beta no existe. Preparar de rama develop a release"
      verificar-ramarelease
      #git checkout release  #git pull origin develop
      touch README-v$version$prerelease.txt
      git add .
      git commit -m "Checkout de rama release para $version$prerelease - version BETA "
      ##git tag -a v$version$prerelease -m "Versión $version$prerelease"
      git log -1 --oneline
      #git checkout develop
      #git merge --no-ff release -m "Merge de actualizacion develop"
      #git log -1 --oneline
      #git pull --all
      #git push --all
      rama=$(git branch --show-current)
      git pull origin $rama
      git push --set-upstream origin $rama
	  source_branch="develop"
	  target_branch="release"
	  url=$(generar_merge_request_url "$source_branch" "$target_branch")
	  echo "----------------------------------------------------------"
	  echo " Pull Request para version pre-release BETA  "
	  echo " URL Solicitud de Fusion: $url"
	  echo "----------------------------------------------------------"
     # Luego de aplicar el Request Merge se etiqueta v.1.2.3-beta.1
   fi
}
#------------------------------------------#
# Preparar y finalizar una versión Master
#------------------------------------------#
function version_master() {
    ultima_version_rama
    read -p "Ingrese la versión (ej. 1.0.0): " version
    git checkout master #git pull
	# git diff master release
	#Already on 'master'
	#Your branch is up to date with 'origin/master'.
	#Auto-merging CHANGELOG.md.orig
	#CONFLICT (add/add): Merge conflict in CHANGELOG.md.orig
	#Auto-merging VERSION.txt.orig
	#CONFLICT (add/add): Merge conflict in VERSION.txt.orig
	#Automatic merge failed; fix conflicts and then commit the result.
	#!/bin/bash
    if git merge --no-ff release; then
	# git merge --no-ff release -m "Merge de rama release a master  (Produccion) "
	 #git tag -a v$version -m "Versión $version"
     git log -1 --oneline
     rama=$(git branch --show-current)
     git pull origin $rama
     git push --set-upstream origin $rama
	 source_branch="release"
	 target_branch="master"
	 url=$(generar_merge_request_url "$source_branch" "$target_branch")
	 echo "----------------------------------------------------------"
	 echo " Pull Request para version release Master  "
	 echo " URL Solicitud de Fusion: $url"
 	 echo "----------------------------------------------------------"
     echo "Fusión exitosa"
	 # Luego de aplicar el Request Merge se etiqueta v.1.2.3
	 
	 #. Fusionar la rama de lanzamiento en master:

	 #. Etiquetar la versión en master:

	#  Fusionar la rama de lanzamiento en develop:
	
	 source_branch="develop"
	 target_branch="release"
	 url=$(generar_merge_request_url "$source_branch" "$target_branch")
	 echo "----------------------------------------------------------"
	 echo " Pull Request para version release Master  "
	 echo " URL Solicitud de Fusion: $url"
 	 echo "----------------------------------------------------------"
	 
    else
		echo "¡Hubo un problema durante la fusión!"
    fi
}
#------------------- -----------------------#
# Muestra ramas fusionadas y no fusionadas
#------------------------------------------#
function show_merged_branches() {
    echo "Ramas fusionadas:"
    git branch --merged | grep -v "\*"
    echo -e "\nRamas no fusionadas:"
    git branch --no-merged | grep -v "\*"
}
#---------------------------#
#  Ver flujo de trabajo
#---------------------------#
function list_branches() {
    git log --oneline --all --decorate --graph
    git branch --list
}
function ultimas_confirm() {
   git log --oneline
}
 
# --- Verificar la existencia de rama master y develop #
verificar_repo() {
  # Verificar si existen las ramas master y develop
  if git show-ref --quiet refs/heads/master && git show-ref --quiet refs/heads/develop; then
    echo "El repositorio ya contiene las ramas master y develop."
    return 0  # Retorna true (0) si las ramas existen
  else
    echo "El repositorio no contiene las ramas master y develop."
    return 1  # Retorna false (1) si las ramas no existen
  fi
}
 
#-------------------------------#
#  Inicializar el repositorio
#-------------------------------#
function init_repo() {
    git init
    git checkout -b master
    git commit --allow-empty -m "Inicio de GitFlow"
    git checkout -b develop
    git config gitflow.prefix.feature "feature"
    git config gitflow.prefix.release "release"
}
 
# --- Mostrar ultima tag y ultimo branch
function ultima_version_rama() {
        echo "------------------------------------"
        echo  " Repositorio local "
        echo "------------------------------------"
	latest_tag=$(git describe --tags --abbrev=0)
	echo "La última etiqueta es: $latest_tag"
	latest_branch=$(git for-each-ref --sort=-committerdate --format '%(refname:short)' refs/heads | head -n 1)
	echo "La última rama es: $latest_branch"
        echo "------------------------------------"
        echo  " Repositorio remoto "
        echo "------------------------------------"
        remote_latest_tag=$(git ls-remote --tags origin | awk '{print $2}' | cut -d '/' -f 3 | sort -r | head -n 1)
        echo "La última etiqueta es: $remote_latest_tag"
        remote_latest_branch=$(git ls-remote --heads origin | awk '{print $2}' | sed 's#refs/heads/##' | sort -r | head -n 1)
        echo "La última rama es: $remote_latest_branch"
}
 
#------------------------------------------#
# Agregar cambios en archivos (staging)
#------------------------------------------#
function agregar_cambios() {
    git add .
    git status
}
function verificar_staging() {
    if [[ -z $(git diff --cached --exit-code) ]]; then
        echo "No hay cambios en el área de preparación (staging area). Añade cambios antes de realizar un commit."
        return 1
    else
        echo "Cambios en el área de preparación detectados. Puedes proceder con el commit."
        return 0
    fi
}
 
function resolver_conflicto_merge() {
  # Configurar git para utilizar vimdiff por defecto
  git config --global merge.tool vimdiff
  git config --global mergetool.vimdiff.cmd 'vimdiff "$BASE" "$LOCAL" "$REMOTE" "$MERGED" -c "wqall"'
  git config --global mergetool.vimdiff.trustExitCode true
  # Ejecutar git pull para obtener los cambios remotos
  #git pull
 
  # Comprobar si hay conflictos
  if [ -n "$(git ls-files --unmerged)" ]; then
    echo "Conflicts found. Resolving conflicts..."
    # Ejecutar git mergetool para iniciar la herramienta de resolución de conflictos (vimdiff)
    git mergetool
    # Comprobar si aún hay conflictos después de la resolución
    if [ -n "$(git ls-files --unmerged)" ]; then
      echo "Conflict resolution failed. Please resolve conflicts manually."
    else
      echo "Conflict resolution successful. Committing changes..."
	  # git add .
      git merge --continue
    fi
  else
    echo "No conflicts found. Nothing to resolve."
  fi
}
# Llamar a la función
#resolve_merge_conflicts
 
 
function generar_merge_request_url() {
  local base_url="http://pdmtdagap01.cl.bsch/TEC00055/NTT_test_repo/merge_requests/new"
  local source_branch="$1"
  local target_branch="$2"
  # Escape branch names (replace characters that are not alphanumeric with %5C)
  local escaped_source_branch="${source_branch//[^a-zA-Z0-9]/%5C}"
  local escaped_target_branch="${target_branch//[^a-zA-Z0-9]/%5C}"
  # Construct the complete URL
  local url="$base_url?merge_request%5Bsource_branch%5D=$escaped_source_branch&merge_request%5Btarget_branch%5D=$escaped_target_branch"
  echo "$url"
}
# Example usage:
#source_branch="feature-J012"
#target_branch="develop"
#url=$(generar_merge_request_url "$source_branch" "$target_branch")
#echo " URL Solicitud de Fusion: $url"
 
 
function git_feat_breaking_change_commit() {
  local mensaje_feat
  local mensaje_breaking_change
  # Asegurarse de estar en una rama
  branch_name=$(git symbolic-ref --short HEAD 2>/dev/null)
  if [ -z "$branch_name" ]; then
    echo "Error: No se encuentra en una rama."
    return 1
  fi
  # Ingresar el mensaje del tipo "feat"
  read -p "Ingrese el mensaje de tipo 'feat': " mensaje_feat
  # Ingresar el mensaje de "BREAKING CHANGE"
  read -p "Ingrese el mensaje de BREAKING CHANGE: " mensaje_breaking_change
  # Hacer un commit con el tipo "feat" y "BREAKING CHANGE"
  git commit -m "feat: $mensaje_feat" -m "BREAKING CHANGE: $mensaje_breaking_change"
  echo "Se realizó un commit con los mensajes de feat y BREAKING CHANGE en la rama $branch_name."
}
 
# Ejemplo de uso:
#git_feat_breaking_change_commit
 
function git_custom_commit() {
  local tipo_commit
  local alcance_commit
  local descripcion_commit
  local cuerpo_mensaje
  local pies_de_pagina
 
  # Asegurarse de estar en una rama
  branch_name=$(git symbolic-ref --short HEAD 2>/dev/null)
  if [ -z "$branch_name" ]; then
    echo "Error: No se encuentra en una rama."
    return 1
  fi
  read -p "Ingrese el tipo de commit (por ejemplo, 'fix', 'feat'): " tipo_commit
  read -p "Ingrese el alcance del commit (opcional): " alcance_commit
  read -p "Ingrese la descripción del commit: " descripcion_commit
  echo "Ingrese el cuerpo del mensaje. Presione Ctrl+D para finalizar."
  cuerpo_mensaje=$(cat)
  echo "Ingrese pies de página, uno por línea. Presione Ctrl+D para finalizar."
  pies_de_pagina=$(cat)
  mensaje_commit="$tipo_commit"
  [ -n "$alcance_commit" ] && mensaje_commit="$mensaje_commit[$alcance_commit]"
  mensaje_commit="$mensaje_commit: $descripcion_commit"
  # Hacer un commit con el mensaje personalizado
  git commit -m "$mensaje_commit" -m "$cuerpo_mensaje" -m "$pies_de_pagina"
  echo "Se realizó un commit con el mensaje personalizado en la rama $branch_name."
}
# Ejemplo de uso:
#git_custom_commit
 
#--------------------------------#
# Crear mensaje de confirmación
#--------------------------------#
function conventional_commit_message() {
    verificar_staging
    if [[ $? -eq 0 ]]; then
        echo "SubMenu de Mensaje Commit"
        echo "Seleccione el tipo de mensaje Conventional Commits:"
        echo "  1. feat: Nueva característica"
        echo "  2. fix: Corrección de errores"
        echo "  3. chore: Tareas de mantenimiento"
        echo "  4. docs: Cambios en la documentación"
        echo "  5. style: Cambios en el estilo del código"
        echo "  6. refactor: Refactorización del código"
        echo "  7. test: Añadir o mejorar pruebas"
        read -p "Seleccione una opción (1-7): " option
        case $option in
            1) type="feat" ;;
            2) type="fix" ;;
            3) type="chore" ;;
            4) type="docs" ;;
            5) type="style" ;;
            6) type="refactor" ;;
            7) type="test" ;;
            *) echo "Opción no válida. Saliendo." && exit 1 ;;
        esac
        read -p "Ingrese una breve descripción: " description
        commit_message="$type: $description"
        echo "Confirmando cambios con el mensaje: \"$commit_message\""
        git commit -m "$commit_message"
        git log -1 --oneline
        #git pull --all
        rama=$(git branch --show-current)
        git pull origin $rama
	git push --set-upstream origin $rama
    fi
}
function submenu_archivos() {
    while true; do
        clear
        echo "============ Submenú de Manejar Archivos ============"
        echo "  1. Agregar cambios en archivos (staging) "
        echo "  2. Crear mensaje de confirmación"
        echo "  3. Mostrar ultimas confirmaciones "
        echo "  4. Volver al menú principal"
        echo "===================================================="
        read -p "Seleccione una opción: " option_archivos
        case $option_archivos in
            1) agregar_cambios ;;
            2) conventional_commit_message ;;
            3) ultimas_confirm ;;
            4) break ;;
            *) echo "Opción no válida. Intente de nuevo." ;;
        esac
        read -p "Presione Enter para continuar..."
        clear
    done
}
 
function inicio()
{
  clear
  echo " BANCO SANTANDER "
  echo "Iniciando ...."
  # Uso de la función verificar_repo
if verificar_repo; then
  echo "El repositorio ya está inicializado."
  #git pull
else
  echo "Inicializando un nuevo repositorio."
  init_repo
fi
read -p "Presione Enter para continuar..."
}
inicio
while true; do
    clear
    echo "================ Menu Principal ================"
    echo "  1. Trabajar en una característica (feature-**) "
    echo "  2. Manejar archivos "
    echo "  3. Generar versión Develop (*-alpha) "
    echo "  4. Generar versión Release (*-beta) "
    echo "  5. Generar versión Master (Prod)"
    echo "  6. Preparar y finalizar Release"
    echo "  7. Ver flujo de trabajo"
    echo "  8. Salir"
    echo "==============================================="
    read -p "Seleccione una opción: " option
    case $option in
        1) version_feature ;;
        2) submenu_archivos ;;
        3) version_develop  ;;
        4) version_release ;;
        5) version_master ;;
        6) show_merged_branches ;;
        7) list_branches ;;
        8) echo "Saliendo del script."; exit 0 ;;
        *) echo "Opción no válida. Intente de nuevo." ;;
    esac
    read -p "Presione Enter para continuar..."
    clear
done