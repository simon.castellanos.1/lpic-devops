#!/bin/bash

# Configurar git para utilizar vimdiff por defecto
git config --global merge.tool vimdiff
git config --global mergetool.vimdiff.cmd 'vimdiff "$BASE" "$LOCAL" "$REMOTE" "$MERGED" -c "wqall"'
git config --global mergetool.vimdiff.trustExitCode true

# Ejecutar git pull para obtener los cambios remotos
git pull

# Comprobar si hay conflictos
if [ -n "$(git ls-files --unmerged)" ]; then
  echo "Conflicts found. Resolving conflicts..."

  # Ejecutar git mergetool para iniciar la herramienta de resolución de conflictos (vimdiff)
  git mergetool

  # Comprobar si aún hay conflictos después de la resolución
  if [ -n "$(git ls-files --unmerged)" ]; then
    echo "Conflict resolution failed. Please resolve conflicts manually."
  else
    echo "Conflict resolution successful. Committing changes..."
    git merge --continue
  fi
else
  echo "No conflicts found. Nothing to resolve."
fi
----

#!/bin/bash

resolve_merge_conflicts() {
  # Configurar git para utilizar vimdiff por defecto
  git config --global merge.tool vimdiff
  git config --global mergetool.vimdiff.cmd 'vimdiff "$BASE" "$LOCAL" "$REMOTE" "$MERGED" -c "wqall"'
  git config --global mergetool.vimdiff.trustExitCode true

  # Ejecutar git pull para obtener los cambios remotos
  git pull

  # Comprobar si hay conflictos
  if [ -n "$(git ls-files --unmerged)" ]; then
    echo "Conflicts found. Resolving conflicts..."

    # Ejecutar git mergetool para iniciar la herramienta de resolución de conflictos (vimdiff)
    git mergetool

    # Comprobar si aún hay conflictos después de la resolución
    if [ -n "$(git ls-files --unmerged)" ]; then
      echo "Conflict resolution failed. Please resolve conflicts manually."
    else
      echo "Conflict resolution successful. Committing changes..."
      git merge --continue
    fi
  else
    echo "No conflicts found. Nothing to resolve."
  fi
}

# Llamar a la función
resolve_merge_conflicts
